import { Route } from '@angular/router';
import { RegisterComponent } from 'app/modules/landing/register/register.component';

export const registerRoutes: Route[] = [
    {
        path     : '',
        component: RegisterComponent
    }
];
