import { Route } from '@angular/router';
import { HomeVisitComponent } from 'app/modules/landing/home-visit/home-visit.component';

export const homeVisitRoutes: Route[] = [
    {
        path     : '',
        component: HomeVisitComponent
    }
];
