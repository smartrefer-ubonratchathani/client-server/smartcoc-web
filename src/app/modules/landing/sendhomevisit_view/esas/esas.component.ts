import { Router, Navigation } from '@angular/router';
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogData } from '../dialog-data';

import { Options } from '@angular-slider/ngx-slider';

@Component({
    selector: 'landing-evaluate-esas',
    templateUrl: './esas.component.html',
    styleUrls: ['./esas.component.css'],
    encapsulation: ViewEncapsulation.None
})

export class EsasDialog {
    userFullname: string;
    lastVisit: string;
    validateForm: boolean = false;
    rowsQuestions: any;

    question_type_id: string = '8';
    question: any[] = [];
    myNgmodel = { items: [] };
    profilePatient: any;
    id: any = {
        question_type_id: 8
    };

    pain_position: string;
    extra_detail: string;
    
    options: Options = {
        disabled : true,
        showTicksValues: true,
        stepsArray: [
            { value: 0, legend: 'ไม่มี' },
            { value: 1, legend: 'น้อย' },
            { value: 2, legend: 'น้อย' },
            { value: 3, legend: 'น้อย' },
            { value: 4, legend: 'ปานกลาง' },
            { value: 5, legend: 'ปานกลาง' },
            { value: 6, legend: 'ปานกลาง' },
            { value: 7, legend: 'รุนแรง' },
            { value: 8, legend: 'รุนแรง' },
            { value: 9, legend: 'รุนแรง' },
            { value: 10, legend: 'รุนแรง' },
        ]
    };

    evaluate :any;
    title : string;

    constructor(
        private router: Router,
        public matDialogRef: MatDialogRef<EsasDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {
        this.title = this.data.evaluate.question_text;
        this.evaluate = this.data.evaluate;
        this.lastVisit = this.data.lastVisit;
        //console.log(this.evaluate);
    }

    close(): void {
        this.matDialogRef.close();
    }

}
