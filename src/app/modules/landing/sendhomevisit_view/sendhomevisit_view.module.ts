import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { SendhomevisitViewComponent } from 'app/modules/landing/sendhomevisit_view/sendhomevisit_view.component';
import { sendhomevisitViewRoutes } from 'app/modules/landing/sendhomevisit_view/sendhomevisit_view.routing';
import { BarthelDialog } from './barthel/barthel.component';
import { AdvanceCarePlanDialog } from './advance-care-plan/advance-care-plan.component';
import { AdlDialog } from './adl/adl.component';
import { EvaluateNtritionDialog } from './evaluate-nutrition/evaluate-nutrition.component';
import { ChildDevelopDialog } from './child-develop/child-develop.component';
import { ChildVisitDialog } from './child-visit/child-visit.component';
import { CvdRiskDialog } from './cvd-risk/cvd-risk.component';
import { ElderlyDialog } from './elderly/elderly.component';
import { EpidemDialog } from './epidem/epidem.component';
import { EsasDialog } from './esas/esas.component';
import { ModifiedRankingDialog } from './modified-ranking/modified-ranking.component';
import { MotorPowerDialog } from './motor-power/motor-power.component';
import { PainKillerDialog } from './pain-killer/pain-killer.component';
import { PostnatalDialog } from './postnatal/postnatal.component';
import { PpsDialog } from './pps/pps.component';
import { PsycheDialog } from './psyche/psyche.component';
import { ScreenNutritionDialog } from './screen-nutrition/screen-nutrition.component';
import { ViolentlyDialog } from './violently/violently.component';
import { OtherDialog } from './other/other.component';

@NgModule({
  declarations: [
    SendhomevisitViewComponent,
    BarthelDialog,
    AdvanceCarePlanDialog,
    AdlDialog,
    EvaluateNtritionDialog,
    ChildDevelopDialog,
    ChildVisitDialog,
    CvdRiskDialog,
    ElderlyDialog,
    EpidemDialog,
    EsasDialog,
    ModifiedRankingDialog,
    MotorPowerDialog,
    PainKillerDialog,
    PostnatalDialog,
    PpsDialog,
    PsycheDialog,
    ScreenNutritionDialog,
    ViolentlyDialog,
    OtherDialog
  ],
  imports     : [
      RouterModule.forChild(sendhomevisitViewRoutes),
      SharedModule
  ]
})
export class SendhomevisitViewModule {

 }
