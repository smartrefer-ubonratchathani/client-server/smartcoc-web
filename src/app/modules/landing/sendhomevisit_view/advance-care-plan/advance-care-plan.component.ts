import { Router, Navigation } from '@angular/router';
import { Component, ViewEncapsulation, Inject } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import { DialogData } from '../dialog-data';


@Component({
    selector: 'landing-evaluate-advanceCarePlan',
    templateUrl: './advance-care-plan.component.html',
    encapsulation: ViewEncapsulation.None
})
export class AdvanceCarePlanDialog {
    userFullname: string;
    lastVisit: string;
    validateForm: boolean = false;
    evaluate :any;
    title : string;
    
    profilePatient: any;
    constructor(
        private router: Router,
        public matDialogRef: MatDialogRef<AdvanceCarePlanDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData,
    ) {
        this.userFullname = localStorage.getItem('userFullname');
        this.profilePatient = JSON.parse(localStorage.getItem('profileData'));
    }

    ngOnInit(): void {
        this.title = this.data.evaluate.question_text;
        this.evaluate = this.data.evaluate;
        this.lastVisit = this.data.lastVisit;
        // this.dataEvaluate();
    }

    close(): void {
        this.matDialogRef.close();
    }
}
