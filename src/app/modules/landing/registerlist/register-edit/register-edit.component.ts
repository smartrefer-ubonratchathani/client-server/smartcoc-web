import { Component, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { RegisterService } from '../../../../services/register.service';
import { ThaiaddressService } from '../../../../services/thaiaddress.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SweetAlertService } from '../../../../shared/sweetalert.service';
import { DateAdapter } from '@angular/material/core';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { EditDialog } from '../../sendhomevisit/edit/edit.component';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

export interface PeriodicElementDrug {
  action: any;
  drugdate: string;
  drugname: string;
  druguse: string;
  drugqty: number;
}

interface Diagnosis {
  code: string;
  name: string;
}

@Component({
  selector: 'landing-home',
  templateUrl: './register-edit.component.html',
  encapsulation: ViewEncapsulation.None
})
export class RegisterEditComponent {

  formControl = new FormControl<string>('');

  itemStorage: any = [];
  itemReferLevel: any = [];
  itemPatientStatus: any = [];
  itemCatagory: any = [];
  refer_no: any;
  sdate: any;
  edate: any;
  validateForm: boolean = false;
  panelOpenState = false;
  refer_level: any;
  patient_status: any;
  refer_cause: any;
  coc_register_id: any;
  cid: any;
  title: any;
  first_name: any;
  middle_name: any;
  last_name: any;
  gender: any;
  gender_name: any;
  dob: any;
  age: any;
  patient_address_name: any;
  phone_number: any;
  diag_text: any;
  refer_hcode: any;
  refer_to_hcode: any;
  evaluate_clause: any;
  extra_detail: any;
  patient_right: any;
  patient_right_code: any;
  discharge_date: any;
  hn: any;
  itemProfile: any = [];
  itemAllergy: any = [];
  itemMedrecconcile: any = [];
  itemService: any = [];
  items: any;
  dataSourceDrug: any = [];
  dataSourceLab: any = [];
  dataSourceXray: any = [];
  dataSourceDrugAllergy: any = [];
  hcode: any = localStorage.getItem('hcode');
  evaluate_clause_check: any = [];
  visit_date: any;

  list_hospital: any = [];
  searchHospital: string = '';
  refer_hospname: string = '';
  refer_to_hospname: string = '';
  is_select_hospcode: boolean = false;

  med_reconcile_checked: any = [];
  lab_checked: any = [];
  xray_checked: any = [];
  allergy_checked: any = [];
  route: any;

  list_all_hospital: any = [];
  filteredHospital: any[] = [];
  is_transfer: boolean = false;

  displayedColumnsDrug: string[] = ['action', 'drugdate', 'drugname', 'druguse', 'drugqty'];
  displayedColumnsLab: string[] = ['action', 'labdate', 'labname', 'labresult', 'labnormal'];
  displayedColumnsXray: string[] = ['action', 'xraydate', 'xrayname', 'xrayresult', 'interpret', 'comments'];
  displayedColumnsDrugAllergy: string[] = ['action', 'drugallergydate', 'drugallergyname', 'drugallergysymptom', 'drugallergylevel', 'drugallergyfind'];

  villageCode: any = '';
  is_smoke: any = '';
  house_no: any;
  department: any = '';
  list_department: any = [];
  icd10: any;
  selectedDiagnosisType: any = '';
  diagnosis: any = [];

  listDiagType = [
    { id: 1, name: 'Principle' },
    { id: 2, name: 'Comorbidity' },
    { id: 3, name: 'Complication' },
    { id: 4, name: 'Other' },
    { id: 5, name: 'External cause' },
  ];
  listItem = [
  ];

  villno: any = '';
  villageName: any = '';

  filteredDiagnosis: Observable<Diagnosis[]>;

  listIcd10Who: any;
  is_reload_icd10: boolean = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private router: Router,
    private registerService: RegisterService,
    private thaiaddressService: ThaiaddressService,
    private spinner: NgxSpinnerService,
    private sweetAlertService: SweetAlertService,
    private dateAdapter: DateAdapter<Date>,
    public dialog: MatDialog,
  ) {
    this.dateAdapter.setLocale('th-TH');
    this.route = sessionStorage.getItem('route');
    let i: any = sessionStorage.getItem('itemStorage');
    this.itemStorage = JSON.parse(i);
    let transfer = sessionStorage.getItem('action');

    if (transfer == 'transfer') {
      this.is_transfer = true;
    }

    this.refer_level = this.itemStorage.refer_level;
    this.patient_status = this.itemStorage.patient_status;
    this.extra_detail = this.itemStorage.extra_detail;
    this.diag_text = this.itemStorage.diag_text;
    this.list_all_hospital = localStorage.getItem('list_all_hospital');
    this.list_all_hospital = JSON.parse(this.list_all_hospital);
    this.listIcd10Who = localStorage.getItem('list_icd10');
    this.listIcd10Who = JSON.parse(this.listIcd10Who);
    let icd10 = sessionStorage.getItem('list_icd10');

    if (!this.listIcd10Who) {
      if(icd10 == null || icd10 == undefined || icd10 == ''){
          this.is_reload_icd10 = true;
      } else {
          this.listIcd10Who = JSON.parse(icd10);
          this.is_reload_icd10 = false;
      }
  } else {
      this.is_reload_icd10 = false;
  }
  }

  async ngOnInit() {
    this.spinner.show();
    await this.list_catagory();
    await this.list_items();
    await this.list_refer_level();
    await this.list_patient_status();
    await this.getHospital();
    await this.getDepartment();
    await this.load_data();
    if(this.is_reload_icd10){
      await this.getIcd10();
  }
    this.spinner.hide();
  }

  async load_data() {
    this.coc_register_id = this.itemStorage.coc_register_id;
    this.cid = this.itemStorage.cid;
    this.title = this.itemStorage.title;
    this.first_name = this.itemStorage.first_name;
    this.middle_name = this.itemStorage.middle_name;
    this.last_name = this.itemStorage.last_name;
    this.gender = this.itemStorage.gender;
    this.gender_name = this.itemStorage.gender_name;
    this.dob = this.itemStorage.dob;
    this.age = this.itemStorage.age;
    this.patient_address_name = this.itemStorage.patient_address_name;
    this.phone_number = this.itemStorage.phone_number;
    this.diag_text = this.itemStorage.diag_text;
    this.refer_hcode = this.itemStorage.refer_hcode;
    this.refer_to_hcode = this.itemStorage.refer_to_hcode;
    this.evaluate_clause = this.itemStorage.evaluate_cause;
    this.patient_right = this.itemStorage.patient_right;
    this.patient_right_code = this.itemStorage.patient_right_code;
    this.discharge_date = this.itemStorage.discharge_date;
    this.visit_date = this.itemStorage.visit_date;
    this.is_smoke = this.itemStorage.is_smoke;
    this.items = this.itemStorage.items;

    let info: any = {
      "coc_register_id": this.itemStorage.coc_register_id
    }
    try{

      await this.registerService.select_regisOne(info).then((res: any) => {
        this.diagnosis = res[0].diag;
        this.dataSourceDrug = res[0].med;
        this.dataSourceLab = res[0].lab;
        this.dataSourceXray = res[0].xray;
        this.dataSourceDrugAllergy = res[0].allergy;
        
      });
    } catch (error) {
      console.log(error);
    }

  }

  // get all hospital
  async getAllHospital() {
    try {
      await this.thaiaddressService.select_all().then((res: any) => {
        this.list_all_hospital = res;
      });
    } catch (error) {
      console.log(error);
    }
    // console.log(this.list_all_hospital);
  }

  // get department
  async getDepartment() {
    try {
      await this.registerService.department().then((res: any) => {
        this.list_department = res;
      });
    } catch (error) {
      console.log(error);
    }
    // console.log(this.list_department);
  }

  // get refer level
  async list_refer_level() {
    try {
      await this.registerService.list_refer_level().then((res: any) => {
        this.itemReferLevel = res;
      });
    } catch (error) {
      console.log(error);
    }
  }

  // get patient status
  async list_patient_status() {
    try {
      await this.registerService.list_patient_status().then((res: any) => {
        this.itemPatientStatus = res;
      });
    } catch (error) {
      console.log(error);
    }
  }

  // get catagory
  async list_catagory() {
    this.evaluate_clause_check = this.itemStorage.evaluate_cause.split(",").map(Number);

    // set select catagory
    try {
      let rs: any = await this.registerService.list_catagory();
      await rs.forEach((data: any, index: any) => {
        var jsonRow: object = data;
        jsonRow['ngModel'] = this.evaluate_clause_check.includes(data.catagory_id);
        this.itemCatagory.push(jsonRow);
      });
    } catch (error) {
      console.log(error);
    }
    // console.log('catagory', this.itemCatagory);
  }

  // get items
  async list_items() {
    if(this.itemStorage.items == null){
      this.items = [];
    } else {
    this.items = this.itemStorage.items.split(",").map(Number);
    }

    // set select items
    try {
      let rs: any = await this.registerService.list_items();
      await rs.forEach((data: any, index: any) => {
        var jsonRow: object = data;
        jsonRow['ngModel'] = this.items.includes(data.id);
        this.listItem.push(jsonRow);
      });
    } catch (error) {
      console.log(error);
    }
    // console.log('items', this.listItem);
  }

  async back() {
    this.router.navigate([this.route]);
  }

  removeItemArray(array, item) {
    for (var i in array) {
      if (array[i] == item) {
        array.splice(i, 1);
        break;
      }
    }
  }

  onCheckboxChange(e) {
    var ev = e;
    if (ev.checked) {
      this.evaluate_clause_check.push(Number(ev.source.value));
    } else {
      this.removeItemArray(this.evaluate_clause_check, Number(ev.source.value));
    }
    this.evaluate_clause_check.map(Number);
    this.evaluate_clause_check.sort();
  }

  async save_coc_register() {
    // console.log("save_coc_register");
    let coc_appointment: any = {};


    if (this.refer_level && this.patient_status) {

      try {
        let rs: any = [];
        
        const diag = this.diagnosis.find((x: any) => x.diag_type == 1);
        let diagnosis_text = '';
        if(diag){
            diagnosis_text = diag.diag_name;
        } else {
            diagnosis_text = this.diag_text;
        }


        if (this.is_transfer) {
          let info: any = {
            "coc_register_id": this.itemStorage.coc_register_id,
            "cid": this.cid,
            "title": this.title,
            "first_name": this.first_name,
            "middle_name": this.middle_name,
            "last_name": this.last_name,
            "gender": this.gender,
            "gender_name": this.gender_name,
            "dob": this.dob,
            "age": this.age,
            "patient_address_name": this.patient_address_name,
            "phone_number": this.phone_number,
            "diag_text": diagnosis_text,
            "refer_hcode": this.refer_hcode,
            "refer_to_pcu": this.refer_to_hcode,
            "refer_level": this.refer_level,
            "patient_status": this.patient_status,
            "refer_cause": '',
            "evaluate_cause": this.evaluate_clause_check.toString(),
            "extra_detail": this.extra_detail,
            "patient_right": this.patient_right,
            "patient_right_code": this.patient_right_code,
            "discharge_date": this.discharge_date,
            "visit_date": this.visit_date,
            "is_smoke": this.is_smoke,
            "items": this.items,
          }
          rs = await this.registerService.update(info, this.coc_register_id);

        } else {
          let info: any = {
            "coc_register_id": this.itemStorage.coc_register_id,
            "cid": this.cid,
            "title": this.title,
            "first_name": this.first_name,
            "middle_name": this.middle_name,
            "last_name": this.last_name,
            "gender": this.gender,
            "gender_name": this.gender_name,
            "dob": this.dob,
            "age": this.age,
            "patient_address_name": this.patient_address_name,
            "phone_number": this.phone_number,
            "diag_text": diagnosis_text,
            "refer_hcode": this.refer_hcode,
            "refer_to_hcode": this.refer_to_hcode,
            "refer_level": this.refer_level,
            "patient_status": this.patient_status,
            "refer_cause": '',
            "evaluate_cause": this.evaluate_clause_check.toString(),
            "extra_detail": this.extra_detail,
            "patient_right": this.patient_right,
            "patient_right_code": this.patient_right_code,
            "discharge_date": this.discharge_date,
            "visit_date": this.visit_date,
            "is_smoke": this.is_smoke,
            "items": this.items,
          }
          rs = await this.registerService.update(info, this.coc_register_id);
        }

        if (rs) {

          /* Delete Diag */
          let rs_del_diag: any = await this.registerService.delete_diag(this.coc_register_id);

          /* บันทึก diag */
          if(rs_del_diag){
            this.diagnosis.forEach(async (e: any) => {
              let data: any = {
                  coc_register_id: this.coc_register_id,
                  diag_code: e.diag_code,
                  diag_name: e.diag_name,
                  diag_type: e.diag_type,
              };
              let rs_diag: any = await this.registerService.save_diag(data);
            });
          }

          this.sweetAlertService.success('คำชี้แจง', 'บันทึกข้อมูลสำเร็จ', 'Smart COC');
          await this.router.navigate([this.route]);
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      this.sweetAlertService.error('คำชี้แจง', 'ไม่ได้ระบุความเร่งด่วน/สถานะผู้ป่วยขณะจำหน่าย', 'Smart COC')
    }
  }

  async med_reconcile(i: any, e: any) {
    // console.log(i);

    if (e.checked) {
      this.med_reconcile_checked.push(i);
    } else {
      this.removeItemArray(this.med_reconcile_checked, i);
    }
  }

  async lab(i: any, e: any) {
    if (e.checked) {
      this.lab_checked.push(i);
    } else {
      this.removeItemArray(this.lab_checked, i);
    }
  }

  async xray(i: any, e: any) {
    if (e.checked) {
      this.xray_checked.push(i);
    } else {
      this.removeItemArray(this.xray_checked, i);
    }
  }

  async allergy(i: any, e: any) {
    if (e.checked) {
      this.allergy_checked.push(i);
    } else {
      this.removeItemArray(this.allergy_checked, i);
    }
  }
  async getHospital() {
    let rs: any = await this.thaiaddressService.select_same_district(
      this.hcode
    );
    this.list_hospital = rs;
    this.filteredHospital = this.list_hospital;
  }
    // search hospital
    search() {

      let is_number: boolean = false;
      if (!isNaN(Number(this.searchHospital))) {
          is_number = true;
      }
      if (this.searchHospital.length == 5 && is_number) {
          this.refer_to_hcode = this.searchHospital;
          let rs: any = this.list_all_hospital.filter((hospital: any) =>
              hospital.code.includes(this.searchHospital)
          );
          if (rs.length == 1) {
              this.refer_to_hcode = rs[0].code;
              this.filteredHospital = rs;
          }
      } else if (!is_number && this.searchHospital.length > 3) {
          //search from lis all hospital
          this.filteredHospital = this.list_all_hospital.filter((hospital: any) =>
              hospital.name.toLowerCase().includes(this.searchHospital.toLowerCase())
          );
          if (this.filteredHospital.length >= 1) {
              this.refer_to_hcode = this.filteredHospital[0].code;
          }

      } else {
          this.refer_to_hospname = '';
          this.filteredHospital = [];
      }

  }


  onSelectionChange(event: any): void {

    this.is_select_hospcode = true;
    this.refer_to_hcode = event.option.value;
  }

  editPatient() {
    const dialogRef = this.dialog.open(EditDialog, {
      width: '480px',
      data: {
        cid: this.cid,
        title: this.title,
        first_name: this.first_name,
        last_name: this.last_name,
        gender_name: this.gender_name,
        patient_address_name: this.patient_address_name,
        phone_number: this.phone_number,
        villageCode: this.villageCode,
        house_no: this.house_no,
      },
    });

    dialogRef.afterClosed().subscribe(async (result) => {
      if (result) {
        let address_his: any;
        const rs: any = await this.thaiaddressService.select_full(
          result.villageCode.substring(0, 2),
          result.villageCode.substring(2, 4),
          result.villageCode.substring(4, 6),
          result.villageCode.substring(6, 8)
        );
        if (rs.length > 0) {
          address_his = rs[0].full_name;
        } else {
          address_his = 'ไม่พบข้อมูล';
        }
        this.patient_address_name = result.house_no + ' ' + address_his;
        this.phone_number = result.phone_number;
        this.villageCode = result.villageCode;
        this.house_no = result.house_no;
        this.villageName = result.villname;
        this.villno = Number(result.villno).toString();
      }
    });
  }

  onSelectionDepartmentChange(event: any): void {
    this.department = event.option.value;
  }



  addDiagnosis() {
    // const value:any = this.formControl.value;
    if (this.icd10 == '' || this.diag_text == '' || this.selectedDiagnosisType == '') {
        this.sweetAlertService.error(
            'คำชี้แจง',
            'กรุณาระบุข้อมูลให้การวินิจฉัย ICD10 ให้ครบถ้วน',
            'SmartRefer Ubon'
        );
        return;
    }
    const icd10code = this.listIcd10Who.find((x: any) => x.name == this.icd10);
    let diag_code = icd10code || { id: '' };

    let diag = {
        diag_code: diag_code.id,
        diag_name: this.diag_text,
        diag_type: this.selectedDiagnosisType
    }
    this.diagnosis.push(diag);
    this.formControl.setValue('');
    this.icd10 = '';
    this.diag_text = '';
    this.selectedDiagnosisType = '';

}

  removeDiagnosis(i: any) {
    this.diagnosis.splice(i, 1); 
    
  }

    // filter icd10
    filterIcd10Who(value: string): void {

      if(value.length >= 3) {
          this.filteredDiagnosis = this.listIcd10Who.filter(icd10 => 
              icd10.name.toLowerCase().includes(value.toLowerCase()) || icd10.id.toLowerCase().includes(value.toLowerCase())
          );    
      }
  }

    // get Icd10
    async getIcd10() {
      try {
          await this.thaiaddressService.getIcd10().then((res: any) => {
              this.listIcd10Who = res;
              sessionStorage.setItem('list_icd10', JSON.stringify(this.listIcd10Who));

          });
      } catch (error) {
          console.log(error);
      }
  }
}
