import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { EvaluateComponent } from 'app/modules/landing/evaluate/evaluate.component';
import { EvaluateRoutes } from 'app/modules/landing/evaluate/evaluate.routing';
import { AdlComponent } from 'app/modules/landing/evaluate/adl/adl.component';
import { BarthelComponent } from 'app/modules/landing/evaluate/barthel/barthel.component';
import { CvdRiskComponent } from '../evaluate/cvd-risk/cvd-risk.component'
import { PsycheComponent } from '../evaluate/psyche/psyche.component';
import { PostnatalComponent } from '../evaluate/postnatal/postnatal.component';
import { ChildVisitComponent } from '../evaluate/child-visit/child-visit.component';
import { EpidemComponent } from '../evaluate/epidem/epidem.component';
import {ChildDevelopmentComponent} from '../evaluate/child-develop/child-develop.component'
import { ElderlyComponent } from '../evaluate/elderly/elderly.component';
import { ViolentlyComponent } from '../evaluate/violently/violently.component';
import { PpsComponent } from '../evaluate/pps/pps.component';
import { EsasComponent } from '../evaluate/esas/esas.component';
import { AdvanceCarePlanComponent } from '../evaluate/advance-care-plan/advance-care-plan.component';
import { EvaluateNutritionComponent } from '../evaluate/evaluate-nutrition/evaluate-nutrition.component';
import { EvaluateSidebarModule } from './evaluate-sidebar/evaluate-sidebar.module';
import { HomeEvaluateComponent } from './home-evaluate/home-evaluate.component';
import { ScreenNutritionComponent } from './screen-nutrition/screen-nutrition.component';
import {MotorPowerComponent} from './motor-power/motor-power.component';
import {ModifiedRankingComponent} from './modified-ranking/modified-ranking.component';
import {PainKillerComponent} from './pain-killer/pain-killer.component'

@NgModule({
    declarations: [
        EvaluateComponent,
        CvdRiskComponent,
        PsycheComponent,
        BarthelComponent,
        ChildVisitComponent,
        EpidemComponent,
        PostnatalComponent,
        ChildDevelopmentComponent,
        ElderlyComponent,
        ViolentlyComponent,
        PpsComponent,
        EsasComponent,
        AdlComponent,
        EvaluateNutritionComponent,
        ScreenNutritionComponent,
        HomeEvaluateComponent,
        AdvanceCarePlanComponent,
        MotorPowerComponent,
        ModifiedRankingComponent,
        PainKillerComponent
    ],
    imports: [
        SharedModule,
        EvaluateRoutes,
        EvaluateSidebarModule
    ]
})
export class EvaluateModule { }
