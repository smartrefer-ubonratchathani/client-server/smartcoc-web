import { Route } from '@angular/router';
import { HomeVisitViewComponent } from 'app/modules/landing/home-visit-view/home-visit-view.component';

export const homeVisitViewRoutes: Route[] = [
    {
        path     : '',
        component: HomeVisitViewComponent
    }
];
