import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { MatPaginatorModule } from '@angular/material/paginator';
import { CocComponent } from 'app/modules/landing/coc/coc.component';
import { cocRoutes } from 'app/modules/landing/coc/coc.routing';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { DialogTransfer } from './dialog-transfer/dialog-transfer.component';
@NgModule({
    declarations: [
        CocComponent,
        DialogTransfer
    ],
    imports     : [
        RouterModule.forChild(cocRoutes),
        MatPaginatorModule,
        SharedModule,
        NgxDaterangepickerMd.forRoot()
    ]
})
export class CocModule
{
}
