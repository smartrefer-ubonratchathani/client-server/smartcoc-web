import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class EvaluateService {

  //   tokenn: any = "";
  httpOptions: any;

  constructor(@Inject('API_URL') private apiUrl: string, private httpClient: HttpClient) {
    // this.token = sessionStorage.getItem('token');
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwcm92Y29kZSI6IjM0IiwicHJvdm5hbWUiOiLguK3guLjguJrguKXguKPguLLguIrguJjguLLguJnguLUiLCJmdWxsbmFtZSI6IuC4mOC4p-C4seC4iuC4iuC4seC4oiDguYHguKrguIfguYDguJTguLfguK3guJkiLCJpYXQiOjE2NzMzMjg0NTcsImV4cCI6MTcwNDg4NjA1N30.EC5XvMBjmKrTKGT2wfu9vnt0j5SNin5O3xpSCuKhbGA'
      })
    };
  }

  async list(info: any): Promise<Object> {
    const _url = `${this.apiUrl}/question/question`;
    return await this.httpClient.post(_url, info).toPromise();
  }

  async saveEvaluate(info: object): Promise<ArrayBuffer> {
    const _url = `${this.apiUrl}/coc_evaluate/insert`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

  async updateEvaluate(id:any,info: object): Promise<ArrayBuffer> {
    const _url = `${this.apiUrl}/coc_evaluate/update?evauate_id=`+id;
    return this.httpClient.put(_url, info, this.httpOptions).toPromise();
  }

  async getEvaluate(info: object) {
    const _url = `${this.apiUrl}/coc_evaluate/select_evaluate`;
    return this.httpClient.post(_url, info, this.httpOptions).toPromise();
  }

}